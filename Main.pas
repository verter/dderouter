unit Main;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls, StdCtrls, Grids, DdeUnit, Math, ComCtrls, DdeMan;

type
  TMainForm = class(TForm)
    StringGrid: TStringGrid;
    StatusBar: TStatusBar;
    Quotes: TDdeServerConv;
    procedure FormCreate(Sender: TObject);
  private
    Tickers: array of array[1..6] of TDdeServerItem;
    TickerRowToName: array[0..65535] of String;
    procedure OnDdePoke(Topic: string; var Action: TPokeAction);
    procedure OnDdeData(Topic: string; Cells: TRect; Data: TVariantTable);
    function GetItemsID(fName: String): Integer;
    { Private declarations }
  public
    { Public declarations }
  end;

var
  MainForm: TMainForm;
  item_names: array[1..6] of String;

implementation

{$R *.dfm}

procedure TMainForm.FormCreate(Sender: TObject);
begin
  DdeExcel := TDdeExcel.Create(self);
  DdeExcel.OnPoke := OnDdePoke;
  DdeExcel.OnData := OnDdeData;

  StringGrid.Cells[0, 0] := 'Symbol';
  StringGrid.Cells[1, 0] := 'Trade';
  StringGrid.Cells[2, 0] := 'Volume';
  StringGrid.Cells[3, 0] := 'Bid';
  StringGrid.Cells[4, 0] := 'Bid size';
  StringGrid.Cells[5, 0] := 'Ask';
  StringGrid.Cells[6, 0] := 'Ask size';

  item_names[1] := 'LAST_';
  item_names[2] := 'TRADESIZE_';
  item_names[3] := 'BID_';
  item_names[4] := 'BIDSIZE_';
  item_names[5] := 'ASK_';
  item_names[6] := 'ASKSIZE_';
  
end;

procedure TMainForm.OnDdePoke(Topic: string; var Action: TPokeAction);
begin
  Action := paAccept;
end;

procedure TMainForm.OnDdeData(Topic: string; Cells: TRect; Data: TVariantTable);
var
  i, j: Integer;
  v: Variant;
  ItemsID: Integer;
begin
  try
    // Adjust grid size
    StringGrid.RowCount := max(StringGrid.RowCount, Cells.Bottom + 2);
    StringGrid.ColCount := max(StringGrid.ColCount, Cells.Right + 1);

    // Update values
    for i := Cells.Top to Cells.Bottom do begin
      for j := Cells.Left to Cells.Right do begin
        v := Data.Cells[i-Cells.Top, j-Cells.Left];

        if j = 0 then begin
          // New ticker name is coming
          TickerRowToName[i] := VarToStr(v);
        end else begin
          ItemsID := GetItemsID(TickerRowToName[i]);
          Tickers[ItemsID][j].Text := VarToStr(v);
        end;

        // Update string grid on form
        if VarType(v) = varString then begin
          StringGrid.Cells[j, i+1] := VarToStr(v);
        end else begin
          StringGrid.Cells[j, i+1] := Data.Cells[i-Cells.Top, j-Cells.Left];
        end;
      end;
    end;
  except
    raise Exception.Create('DDE data formatting error');
  end;

  // Time of last update
  StatusBar.SimpleText := 'Last update: ' + TimeToStr(Time());
end;


function TMainForm.GetItemsID(fName: String): Integer;
var
  i, j: Integer;
  exists: Boolean;
begin
  Result := -1;
  exists := False;
  for i := 0 to Length(Tickers) - 1 do begin
    if (Tickers[i][1] as TDdeServerItem).Name = 'LAST_' + fName then begin
      exists := True;
      Result := i;
    end;
  end;
  if not exists then begin
    SetLength(Tickers, Length(Tickers) + 1);
    i := Length(Tickers) - 1;

    for j := 1 to 6 do begin
      Tickers[i][j] := TDdeServerItem.Create(Self);
      Tickers[i][j].ServerConv := Quotes;
      Tickers[i][j].Name := item_names[j] + fName;
    end;

    Result := i;
  end;
end;

end.
